package com.ruoyi.project.es.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:31 PM
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("all")
public class QueryCondition {

	@ApiModelProperty(value = "查询字段")
	private String field;
	@ApiModelProperty(value = "查询规则")
	private String rule;
	@ApiModelProperty(value = "查询值")
	private String val;
}
