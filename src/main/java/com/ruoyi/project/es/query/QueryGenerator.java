package com.ruoyi.project.es.query;

import com.github.pagehelper.PageHelper;
import com.ruoyi.project.es.domain.PageSearchVO;
import com.ruoyi.project.es.enums.QueryRuleEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:03 PM
 */
@SuppressWarnings("all")
public class QueryGenerator {
	public static final String FULL_MATCH_FLAGFUL = "\"";
	public static final String LIKE_MATCH_FLAGFUL = "*";

	/**
	 * 初始化请求
	 *
	 * @param pageReq
	 */
	public static NativeSearchQueryBuilder initSearchQuery(PageSearchVO pageReq) {
		NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();
		initPageQuery(searchQuery, pageReq);
		initSelectQuery(searchQuery, pageReq);
		initSuperQuery(searchQuery, pageReq);
		initOrder(searchQuery, pageReq);
		initHighlightQuery(searchQuery, pageReq);
		return searchQuery;
	}

	/**
	 * 高亮查询
	 *
	 * @param searchQuery
	 * @param pageReq
	 */
	private static void initHighlightQuery(NativeSearchQueryBuilder searchQuery, PageSearchVO pageReq) {
		List<String> highlightFields = pageReq.getHighlightFields();
		if (CollectionUtils.isNotEmpty(highlightFields)) {
			HighlightBuilder highlightBuilder = new HighlightBuilder();
			boolean customTag = StringUtils.isNoneBlank(pageReq.getPreTag()) && StringUtils.isNoneBlank(pageReq.getPostTag());

			if (customTag) {
				// 自定义高亮标签
				highlightBuilder.preTags(pageReq.getPreTag())
						.postTags(pageReq.getPostTag());
			}
			highlightFields.forEach(t -> highlightBuilder.field(t));
			searchQuery.withHighlightBuilder(highlightBuilder);
		}
	}

	/**
	 * 排序
	 *
	 * @param searchQuery
	 * @param pageReq
	 */
	private static void initOrder(NativeSearchQueryBuilder searchQuery, PageSearchVO pageReq) {
		List<QueryOrder> orderList = pageReq.getOrderList();
		if (CollectionUtils.isNotEmpty(orderList)) {
			orderList.parallelStream()
					.filter(t -> StringUtils.isNotEmpty(t.getField()) && Objects.nonNull(t.getIsAsc()))
					.forEach(t -> {
						FieldSortBuilder sortBuilder = SortBuilders.fieldSort(t.getField()).order(t.getIsAsc() ? SortOrder.ASC : SortOrder.DESC);
						searchQuery.withSort(sortBuilder);
					});
		}
	}

	/**
	 * 高级查询
	 *
	 * @param searchQuery
	 * @param pageReq
	 */
	private static void initSuperQuery(NativeSearchQueryBuilder searchQuery, PageSearchVO pageReq) {

		BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
		// 查询条件
		List<QueryCondition> queryList = pageReq.getQueryList();

		if (!CollectionUtils.isEmpty(queryList)) {

			queryList.parallelStream()
					.filter(t -> StringUtils.isNoneBlank(t.getField())
							&& StringUtils.isNoneBlank(t.getVal()))
					.forEach(t -> {
						//TODO 构建查询语句
						getOpSQL(queryBuilder, t);
					});

		}

		String keyword = pageReq.getKeyword();
		if (StringUtils.isNoneBlank(keyword)) {
			BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
			boolQueryBuilder.should(QueryBuilders.queryStringQuery(keyword));

			//小写
			boolQueryBuilder.should(QueryBuilders.queryStringQuery(keyword.toUpperCase()));
			Boolean enableWordLike = pageReq.getEnableWordLike();
			Boolean enableWordFull = pageReq.getEnableWordFull();
			if (enableWordFull
					//排除开头结尾\的
					&& !(keyword.startsWith(FULL_MATCH_FLAGFUL)
					&& keyword.endsWith(FULL_MATCH_FLAGFUL))
			) {
				//全文匹配
				boolQueryBuilder.should(QueryBuilders.queryStringQuery(StringUtils.join(FULL_MATCH_FLAGFUL, keyword, LIKE_MATCH_FLAGFUL)));

			}

			if (enableWordFull
					//排除开头结尾\的
					&& !(keyword.startsWith(LIKE_MATCH_FLAGFUL)
					&& keyword.endsWith(LIKE_MATCH_FLAGFUL))
			) {
				//模糊
				boolQueryBuilder.should(QueryBuilders.queryStringQuery(StringUtils.join(LIKE_MATCH_FLAGFUL, keyword, LIKE_MATCH_FLAGFUL)));
				//小写模糊
				boolQueryBuilder.should(QueryBuilders.queryStringQuery(StringUtils.join(LIKE_MATCH_FLAGFUL, keyword.toLowerCase(), LIKE_MATCH_FLAGFUL)));

			}
			queryBuilder.must(boolQueryBuilder);

		}
		searchQuery.withQuery(queryBuilder);
	}

	private static void getOpSQL(BoolQueryBuilder queryBuilder, QueryCondition queryCondition) {
		String field = queryCondition.getField();
		String rule = queryCondition.getRule();
		String val = queryCondition.getVal();
		QueryRuleEnum ruleEnum = QueryRuleEnum.getRule(rule);
		if (ruleEnum == null) {
			return;
		}

		switch (ruleEnum) {
			case EQ:
				queryBuilder.must(QueryBuilders.matchQuery(field, val).operator(Operator.AND));
				break;

			case NE:
				queryBuilder.mustNot(QueryBuilders.matchQuery(field, val).operator(Operator.AND));
				break;
			case GT:
				queryBuilder.must(QueryBuilders.rangeQuery(field).gt(val));
				break;
			case GE:
				queryBuilder.must(QueryBuilders.rangeQuery(field).gte(val));
				break;
			case LT:
				queryBuilder.must(QueryBuilders.rangeQuery(field).lt(val));
				break;
			case LE:
				queryBuilder.must(QueryBuilders.rangeQuery(field).lte(val));
				break;
			case IN:
				BoolQueryBuilder inBuilders = QueryBuilders.boolQuery();
				str2List(val).forEach(t -> {
					inBuilders.should(QueryBuilders.matchQuery(field, t).operator(Operator.AND));
				});
				queryBuilder.must(inBuilders);
				break;
			case NOTIN:
				BoolQueryBuilder notInBuilders = QueryBuilders.boolQuery();
				str2List(val).forEach(t -> {
					notInBuilders.should(QueryBuilders.matchQuery(field, t).operator(Operator.AND));
				});
				queryBuilder.mustNot(notInBuilders);
				break;
			case LIKE:
				queryBuilder.must(QueryBuilders.wildcardQuery(field, StringUtils.join(LIKE_MATCH_FLAGFUL, val, LIKE_MATCH_FLAGFUL)));
				break;
			case LEFT_LIKE:
				queryBuilder.must(QueryBuilders.queryStringQuery(StringUtils.join(LIKE_MATCH_FLAGFUL, val)));
				break;
			case RIGHT_LIKE:
				queryBuilder.must(QueryBuilders.queryStringQuery(StringUtils.join(val, LIKE_MATCH_FLAGFUL)));
				break;
			case BETTWEEN:
				String[] vals = StringUtils.split(val, ",", 2);
				if (vals.length != 2) {
					break;
				}
				queryBuilder.must(QueryBuilders.rangeQuery(field).gte(vals[0]).lte(vals[1]));
				break;
			case NOTBETTWEEN:
				String[] _vals = StringUtils.split(val, ",", 2);
				if (_vals.length != 2) {
					break;
				}
				queryBuilder.mustNot(QueryBuilders.rangeQuery(field).gte(_vals[0]).lte(_vals[1]));
				break;
			case ISNULL:
				queryBuilder.mustNot(QueryBuilders.existsQuery(field));
				break;
			case NOTNULL:
				queryBuilder.must(QueryBuilders.existsQuery(field));
				break;
			default:
		}


	}

	/**
	 * 查询的字段
	 *
	 * @param searchQuery
	 * @param pageReq
	 */
	private static void initSelectQuery(NativeSearchQueryBuilder searchQuery, PageSearchVO pageReq) {
		List<String> selectFields = pageReq.getSelectFields();
		if (CollectionUtils.isNotEmpty(selectFields)) {
			String[] colums = selectFields.parallelStream().toArray(String[]::new);
			searchQuery.withFields(colums);
		}
	}

	/**
	 * 分页查询
	 *
	 * @param searchQuery
	 * @param pageReq
	 */
	private static void initPageQuery(NativeSearchQueryBuilder searchQuery, PageSearchVO pageReq) {
		Integer pageNum = 0;
		Integer pageSize = 100;

		if (PageHelper.getLocalPage() == null) {
			pageNum = PageHelper.getLocalPage().getPageNum();
			pageSize = PageHelper.getLocalPage().getPageSize() - 1;
		}
		Pageable pageble = PageRequest.of(pageNum, pageSize);
		searchQuery.withPageable(pageble);

	}

	private static List str2List(String value) {
		String[] searchList = {"，"," "};
		String[] replacementList = {",",","};
		//把空格和中文逗号全部替换为英文逗号
		String str = StringUtils.replaceEachRepeatedly(value, searchList, replacementList);
		//转成数组
		return Arrays.asList(StringUtils.split(str, ","));
	}
}
