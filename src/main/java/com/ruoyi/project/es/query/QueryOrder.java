package com.ruoyi.project.es.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:31 PM
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("all")
public class QueryOrder {

	@ApiModelProperty(value = "排序字段")
	private String field;

	@ApiModelProperty(value = "是否升序")
	private Boolean isAsc;

}
