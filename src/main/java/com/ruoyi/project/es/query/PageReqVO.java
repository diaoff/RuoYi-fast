package com.ruoyi.project.es.query;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Optional;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:29 PM
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("all")
public class PageReqVO {


	@ApiModelProperty(value = "查询条件")
	protected List<QueryCondition> queryList;
	@ApiModelProperty(value = "排序条件")
	protected List<QueryOrder> orderList;
	@ApiModelProperty(value = "显示字段")
	protected List<String> selectFields;
	@ApiModelProperty(hidden = true)
	protected String dataScope;
	@ApiModelProperty(hidden = true)
	protected Object[] params;
	@ApiModelProperty(value = "页码")
	private Integer pageNum;
	@ApiModelProperty(value = "每页数量")
	private Integer pageSize;


	public <T> T getFirstParam(Class<T> clz) {
		return Optional.ofNullable(params)
				.map(t -> {
					if (t.length == 0) {
						return null;
					}
					return params[0];
				}).map(t -> {
					T obj = JSON.parseObject(JSON.toJSONString(t), clz);
					return obj;
				}).orElse(null);
	}
}
