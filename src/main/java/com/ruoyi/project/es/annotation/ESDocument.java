package com.ruoyi.project.es.annotation;

import com.ruoyi.project.es.domain.DocumentBase;

import java.lang.annotation.*;

/**
 * @author diaoff
 * @version 1.0 2022/4/21
 * @date: 2022/4/21 10:37 AM
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ESDocument {


	/**
	 * 文档类型
	 * @return
	 */
	Class<? extends DocumentBase> value();
}
