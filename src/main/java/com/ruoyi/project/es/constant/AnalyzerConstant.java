package com.ruoyi.project.es.constant;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 5:59 PM
 */
@SuppressWarnings("all")
public class AnalyzerConstant {
	/**
	 * hanlp默认分词
	 */
	public static final String HANLP = "hanlp";

	/**
	 * hanlp索引分词
	 */
	public static final String HANLP_INDEX = "hanlp_index";

	/**
	 * ik最细粒度的拆分
	 */
	public static final String IK = "ik_max_word";

	/**
	 * ik_smart最粗粒度的拆分
	 */
	public static final String IK_SMART = "ik_smart";
}
