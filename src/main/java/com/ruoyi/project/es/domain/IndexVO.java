package com.ruoyi.project.es.domain;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 5:55 PM
 */
@Data
@Accessors(chain = true)
@SuppressWarnings("all")
public class IndexVO {

	/**
	 * 索引名称
	 */
	private String indexName;

	/**
	 * 是否已经创建
	 */
	private Boolean created;
}
