package com.ruoyi.project.es.domain;

import com.ruoyi.project.es.query.PageReqVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 5:55 PM
 */
@Data
@SuppressWarnings("all")
public class PageSearchVO extends PageReqVO {
	@ApiModelProperty(value = "全局搜索字符")
	private String keyword;
	@ApiModelProperty(value = "可高亮显示字段")
	private List<String> highlightFields;
	@ApiModelProperty(value = "高亮显示前缀")
	private String preTag;
	@ApiModelProperty(value = "高亮显示后缀")
	private String postTag;
	@ApiModelProperty(value = "启用全局模糊")
	private Boolean enableWordLike = Boolean.TRUE;
	@ApiModelProperty(value = "启用全字匹配")
	private Boolean enableWordFull = Boolean.TRUE;

}
