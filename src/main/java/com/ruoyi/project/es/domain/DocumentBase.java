package com.ruoyi.project.es.domain;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;
import java.util.Map;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 5:54 PM
 */
@SuppressWarnings("all")
public abstract class DocumentBase {

	/**
	 * 高亮数据
	 */
	private Map<String, List<String>> highlightData;


	@Field(type = FieldType.Keyword, index = false)
	protected String module;

	@Field(type = FieldType.Keyword, index = false)
	protected String func;


	public Object getPkId() {
		return null;
	}

	public void setPkId(Object id) {
	}

	public Map<String, List<String>> getHighlightData() {
		return highlightData;
	}

	public void setHighlightData(Map<String, List<String>> highlightData) {
		this.highlightData = highlightData;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getFunc() {
		return func;
	}

	public void setFunc(String func) {
		this.func = func;
	}

	public Boolean convert() {
		return Boolean.TRUE;
	}
}
