package com.ruoyi.project.es.enums;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:02 PM
 */
@SuppressWarnings("all")
public enum ESUpdateTypeEnum {

	save,
	insert,
	insertWithPk,
	insertBatchWithPk,
	saveOrUpdate,
	updateById,
	deleteById,
	deleteByIds,
	update,
	delete,
	logicDeleteById,
	logicDeleteByIds;


	/**
	 * 是否是删除
	 * 用于在拦截器中判断是否需要删除索引
	 * @param type
	 * @return
	 */
	public static Boolean isDelete(ESUpdateTypeEnum type){
		return type == save || type == update || type == delete;
	}

}
