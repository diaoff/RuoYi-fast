package com.ruoyi.project.es.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 查询规则枚举
 *
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 8:03 PM
 */
@Getter
@AllArgsConstructor
@SuppressWarnings("all")
public enum QueryRuleEnum {

	GT("gt", "大于"),
	GE("ge", "大于等于"),
	LT("lt", "小于"),
	LE("le", "小于等于"),
	EQ("eq", "等于"),
	NE("ne", "不等于"),
	IN("in", "包含"),
	NOTIN("notIn", "不包含"),
	LIKE("like", "全模糊"),
	LEFT_LIKE("leftLike", "左模糊"),
	RIGHT_LIKE("rightLike", "右模糊"),
	BETTWEEN("between", "区间"),
	NOTBETTWEEN("notBetween", "不区间"),
	ISNULL("isNull", "为空"),
	NOTNULL("notNull", "不为空");


	private String rule;

	private String msg;

	public static QueryRuleEnum getByValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}
		for (QueryRuleEnum val : values()) {
			if (val.getRule().equals(value)) {
				return val;
			}
		}
		return null;
	}

	public static QueryRuleEnum getRule(String rule) {
		if (StringUtils.isBlank(rule)) {
			return null;
		}
		for (QueryRuleEnum val : values()) {
			if (val.getRule().equals(rule)) {
				return val;
			}
		}
		return null;
	}


}
