package com.ruoyi.project.es.event;

import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import com.ruoyi.project.es.enums.ESUpdateTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:02 PM
 */
@Getter
@Setter
@SuppressWarnings("all")
public class ESDocumentEvent extends ApplicationEvent {
	private String key;
	private IWrapperMapper mapper;
	private Object documentEntity;
	private ESUpdateTypeEnum type;


	public ESDocumentEvent(Object source, String key, IWrapperMapper mapper, Object documentEntity, ESUpdateTypeEnum type) {
		super(source);
		this.key = key;
		this.mapper = mapper;
		this.documentEntity = documentEntity;
		this.type = type;
	}
}
