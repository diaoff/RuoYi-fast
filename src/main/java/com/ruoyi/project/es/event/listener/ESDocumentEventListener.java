package com.ruoyi.project.es.event.listener;

import cn.hutool.core.util.ArrayUtil;
import com.ruoyi.project.es.domain.DocumentBase;
import com.ruoyi.project.es.enums.ESUpdateTypeEnum;
import com.ruoyi.project.es.event.ESDocumentEvent;
import com.ruoyi.project.es.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Collection;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:03 PM
 */
@Component
@SuppressWarnings("all")
public class ESDocumentEventListener {

	@Autowired
	private DocumentService documentService;


	@TransactionalEventListener(fallbackExecution = true, classes = {ESDocumentEvent.class})
	@Async
	@Order(0)
	public void onApplicationEvent(ESDocumentEvent event) {
		Object documentEntity = event.getDocumentEntity();
		if (documentEntity == null) {
			return;
		}
		if (ESUpdateTypeEnum.isDelete(event.getType())) {
			//是更新操作
			if (event.getMapper() == null) {
				return;
			}
			Class clz = event.getMapper().entityClass();
			DocumentBase document = documentService.getDocument(event.getKey(), clz);

			if (document == null) {
				return;
			}
			Class<? extends DocumentBase> documentClz = document.getClass();
			if (documentEntity instanceof Long) {
				//只有删除时,入参数据是Long类型
				documentService.delete(((Long) documentEntity)
						.toString(), documentClz);
				return;
			}
			if (ArrayUtil.isArray(documentEntity)) {
				Object[] ids = (Object[]) documentEntity;
				for (Object id : ids) {
					if (id instanceof Long) {
						documentService.delete(id.toString(), documentClz);
					}
				}
			} else if (documentEntity instanceof Collection) {
				Collection coll = (Collection) documentEntity;
				coll.forEach(id -> {
					if (id instanceof Long) {
						documentService.delete(id.toString(), documentClz);
					}
				});
			}
			return;
		}

		if (documentEntity instanceof Collection) {
			//批量插入 OR 更新的情况
			Collection coll = (Collection) documentEntity;
			coll.forEach(obj -> {
				DocumentBase document =
						documentService.getDocument(event.getKey(), obj);
				documentService.save(document);
			});
			return;
		}

		//最后是单个插入 OR 更新的情况
		DocumentBase document = documentService.getDocument(event.getKey(), documentEntity);
		documentService.save(document);

	}
}
