package com.ruoyi.project.es.aspect;

import cn.hutool.core.util.EnumUtil;
import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import com.ruoyi.project.es.cache.ESClassCache;
import com.ruoyi.project.es.enums.ESUpdateTypeEnum;
import com.ruoyi.project.es.event.ESDocumentEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.annotation.Order;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:00 PM
 */
@Aspect
@Order(99)
@Component
@Slf4j
@SuppressWarnings("all")
public class ESUpdateAspect {

	private static final Map<String, ESUpdateTypeEnum> typeMap = EnumUtil.getEnumMap(ESUpdateTypeEnum.class);

	@Autowired
	private ApplicationEventPublisher publisher;

	@Autowired
	private ElasticsearchRestTemplate restTemplate;

	@Pointcut("execution(public * com.ruoyi.project.*.mapper..*.*(..))")
	public void doSave() {

	}

	@Around("doSave()")
	public Object processDoSave(ProceedingJoinPoint joinPoint) throws Throwable {
		Object res = joinPoint.proceed();

		String method = joinPoint.getSignature().getName();

		if (!typeMap.containsKey(method)) {
			//不需要操作
			return res;
		}
		String key = StringUtils.join(joinPoint.getTarget()
				.getClass().getName(), ".", method);
		if (!ESClassCache.exists(key)) {
			return res;
		}
		if (joinPoint.getArgs() == null || joinPoint.getArgs().length == 0) {
			return res;
		}

		//返回类型
		boolean returnType = res instanceof Integer || res instanceof Boolean;

		if (!returnType) {
			return res;
		}

		IWrapperMapper mapper = null;

		if (joinPoint.getTarget() instanceof IWrapperMapper) {
			mapper = (IWrapperMapper) joinPoint.getTarget();
		}
		publisher.publishEvent(new ESDocumentEvent(this, key, mapper, joinPoint.getArgs()[0], typeMap.get(method)));
		return res;
	}
}
