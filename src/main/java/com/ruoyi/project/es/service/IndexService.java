package com.ruoyi.project.es.service;

import com.ruoyi.project.es.domain.IndexVO;
import org.elasticsearch.cluster.metadata.MappingMetadata;
import org.springframework.data.elasticsearch.core.IndexInformation;

import java.util.List;
import java.util.Map;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:04 PM
 */

@SuppressWarnings("all")
public interface IndexService {

	/**
	 * 获取索引clz
	 *
	 * @param className
	 * @return
	 */
	public Class<?> getIndexClass(String className);

	/**
	 * 查询索引列表
	 */
	public List<IndexVO> selectIndexList();

	/**
	 * 获取索引映射
	 *
	 * @return
	 */
	public Map<String, MappingMetadata> getIndexMapping(String indexName);

	/**
	 * 索引详情
	 */
	public List<IndexInformation> selectIndexById(String indexName);

	/**
	 * 创建索引
	 */
	public Boolean createIndex(String indexName);

	/**
	 * 删除索引
	 */
	public Boolean deleteIndex(String indexName);
}
