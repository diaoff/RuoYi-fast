package com.ruoyi.project.es.service.impl;

import cn.hutool.core.util.ClassUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.ruoyi.project.es.domain.IndexVO;
import com.ruoyi.project.es.service.IndexService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.math3.exception.NoDataException;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.indices.GetMappingsRequest;
import org.elasticsearch.cluster.metadata.MappingMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.IndexInformation;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:05 PM
 */
@Service
@Slf4j
@SuppressWarnings("all")
public class IndexServiceImp implements IndexService {

	@Autowired
	private ElasticsearchRestTemplate restTemplate;
	/**
	 * 是否自动创建索引
	 *
	 * @param indexVO
	 * @return
	 */
	private Boolean autoCreateIndex = Boolean.TRUE;

	private static Map<String, Class> indexCache = Maps.newConcurrentMap();

	private Set<Class<?>> getIndexClassList() {
		Set<Class<?>> classes = ClassUtil.scanPackageByAnnotation("com.ruoyi.project", Document.class);
		classes.forEach(clazz -> {
			indexCache.put(clazz.getSimpleName(), clazz);
		});
		return classes;
	}

	@Override
	public Class<?> getIndexClass(String className) {

		return indexCache.computeIfAbsent(className, key -> {
			Set<Class<?>> indexClassList = getIndexClassList();
			return indexClassList.parallelStream().filter(clz -> {
						//获取注解上索引名称
						Document annotation = clz.getAnnotation(Document.class);
						String indexName = annotation.indexName();
						//类名相同或注解上索引名称相同
						return clz.getSimpleName().equals(indexName) || indexName.equals(className);
					})
					//获取第一个,否则抛出异常
					.findFirst().orElseThrow(NoDataException::new);
		});

	}

	/**
	 * bean加载后执行
	 */
	@PostConstruct
	void loadDocumentIndex() {
		if (!autoCreateIndex) {
			return;
		}
		Set<Class<?>> indexClassList = getIndexClassList();
		//创建索引
		indexClassList.forEach(clazz -> {
			createIndex(clazz.getSimpleName());
		});
	}

	@Override
	public List<IndexVO> selectIndexList() {
		List<IndexVO> indexs = Lists.newArrayList();
		Set<Class<?>> indexClassList = getIndexClassList();
		indexClassList.parallelStream().forEach(clz -> {
			IndexVO indexVO = new IndexVO();
			Document annotation = clz.getAnnotation(Document.class);
			String indexName = annotation.indexName();
			//查询索引是否已经创建
			boolean exists = restTemplate.indexOps(IndexCoordinates.of(indexName)).exists();
			indexVO.setIndexName(indexName).setCreated(exists);
			indexs.add(indexVO);
		});

		return indexs;
	}

	@Override
	public Map<String, MappingMetadata> getIndexMapping(String indexName) {
		Class<?> clz = getIndexClass(indexName);
		IndexOperations idx = restTemplate.indexOps(clz);
		try {
			GetMappingsRequest request = new GetMappingsRequest();
			request.indices(idx.getIndexCoordinates().getIndexName());
			return restTemplate.execute(client->{
				Map mappings = client.indices()
						.getMapping(request,RequestOptions.DEFAULT)
						.mappings();
				if (MapUtils.isEmpty(mappings)) {
					return Maps.newHashMap();
				}
				return mappings;
			});

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<IndexInformation> selectIndexById(String indexName) {

		Class<?> clz = getIndexClass(indexName);
		IndexOperations idx = restTemplate.indexOps(clz);
		try {
			return idx.getInformation();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Boolean createIndex(String indexName) {
		Class<?> clz = getIndexClass(indexName);
		try {
			IndexOperations idx = restTemplate.indexOps(clz);
			if(!idx.exists()){
				//索引不存在,执行创建索引
				idx.create();
			}
			idx.putMapping();
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return Boolean.TRUE;
	}

	@Override
	public Boolean deleteIndex(String indexName) {
		Class<?> clz = getIndexClass(indexName);
		try {
			IndexOperations idx = restTemplate.indexOps(clz);
			if(idx.exists()){
				//索引存在,执行索引删除
				idx.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Boolean.TRUE;
	}
}
