package com.ruoyi.project.es.service.impl;

import cn.hutool.extra.spring.SpringUtil;
import cn.org.atool.fluent.mybatis.base.crud.IQuery;
import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import com.github.pagehelper.PageHelper;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.es.annotation.ESDocument;
import com.ruoyi.project.es.cache.ESClassCache;
import com.ruoyi.project.es.domain.DocumentBase;
import com.ruoyi.project.es.domain.PageSearchVO;
import com.ruoyi.project.es.query.QueryGenerator;
import com.ruoyi.project.es.service.DocumentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:05 PM
 */
@Service
@Slf4j
@SuppressWarnings("all")
public class DocumentServiceImp implements DocumentService {

	@Autowired
	private ElasticsearchRestTemplate restTemplate;

	private final String appName = "jandan";

	@Override
	public <T> TableDataInfo<List<T>> selectDocument(PageSearchVO pageRequest, Class<T> clz) {
		NativeSearchQueryBuilder builer = QueryGenerator.initSearchQuery(pageRequest);
		NativeSearchQuery query = builer.build();
		SearchHits<T> search = restTemplate.search(query, clz);
		List<SearchHit<T>> list = search.getSearchHits();
		if (CollectionUtils.isEmpty(list)) {
			return new TableDataInfo(Lists.newArrayList(), 0);
		}
		List<T> data = list.parallelStream().filter(t -> {
			DocumentBase document = (DocumentBase) t.getContent();
			document.setHighlightData(t.getHighlightFields());
			return Boolean.TRUE;
		}).map(SearchHit::getContent).collect(Collectors.toList());
		TableDataInfo result = new TableDataInfo(data, search.getTotalHits());
		result.setCode(HttpStatus.OK.value());
		return result;
	}

	@Override
	public <T> TableDataInfo<List<T>> selectDocument(PageSearchVO pageRequest) {
		NativeSearchQueryBuilder builer = QueryGenerator.initSearchQuery(pageRequest);
		NativeSearchQuery query = builer.build();
		SearchHits<Map> search = restTemplate.search(query, Map.class, IndexCoordinates.of("*"));
		List<SearchHit<Map>> list = search.getSearchHits();
		if (CollectionUtils.isEmpty(list)) {
			return new TableDataInfo(Lists.newArrayList(), 0);
		}
		List<Map> data = list.parallelStream().filter(t -> {
			t.getContent().put("highlightData", t.getHighlightFields());
			return Boolean.TRUE;
		}).map(SearchHit::getContent).collect(Collectors.toList());
		TableDataInfo result = new TableDataInfo(data, search.getTotalHits());
		result.setCode(HttpStatus.OK.value());
		return result;
	}

	@Override
	public void save(DocumentBase document) {
		try {
			if (document == null) {
				return;
			}
			Boolean flag = document.convert();
			if (!flag) {
				// 如果转换失败，则不保存
				return;
			}
			restTemplate.save(document);
			if (log.isDebugEnabled()) {
				log.debug("保存成功，文档：{}", document);
			}
		} catch (Exception e) {
			log.error("保存文档失败，文档信息：{}", document, e);
		}

	}

	@Override
	public void delete(DocumentBase document) {
		try {
			if (document == null) {
				return;
			}
			if (document.getPkId() == null) {
				restTemplate.delete(document);
				if (log.isErrorEnabled()) {
					log.debug("删除成功，文档：{}", document);
				}
			}
		} catch (Exception e) {
			log.error("删除文档失败，文档信息：{}", document, e);
		}
	}

	@Override
	public void delete(String id, Class clz) {
		try {
			if (id == null) {
				return;
			}
			restTemplate.delete(id, clz);
		} catch (Exception e) {
			log.error("删除文档失败，文档id：{}", id, e);
		}
	}

	@Override
	public DocumentBase getDocument(String key, Object obj) {
		try {
			if (key == null || obj == null) {
				return null;
			}
			Class documentClz = ESClassCache.getCacheDocumentClass(key);
			ESDocument annotation = null;
			if (documentClz == null) {
				ESClassCache.put(key, null);
				if (obj instanceof Class) {
					//实例化,再获取注解
					annotation = ((Class<?>) obj).getDeclaredConstructor().newInstance().getClass().getAnnotation(ESDocument.class);
				} else {
					annotation = obj.getClass().getAnnotation(ESDocument.class);
				}
			}

			if (annotation == null) {
				return null;
			}
			documentClz = annotation.value();
			ESClassCache.put(key, documentClz);
			//反射实例化
			Object document = documentClz.getDeclaredConstructor().newInstance();
			if (!(document instanceof DocumentBase)) {
				return null;
			}
			BeanUtils.copyProperties(obj, document);
			DocumentBase doc = (DocumentBase) document;

			if (StringUtils.isBlank(doc.getModule())) {
				doc.setModule(appName);
			}

			if (StringUtils.isBlank(doc.getFunc())) {
				//doc.setFunc(LogControllerConsoleAspect.getControllerMethodName());
			}

			return doc;
		} catch (Exception e) {
			log.error("获取文档失败，文档信息：{}", obj, e);
		}
		return null;
	}

	@Override
	public void init(List<String> entityNames, List<Long> ids) {
		Set<String> temSet = null;
		if (CollectionUtils.isNotEmpty(entityNames)) {
			temSet = Sets.newHashSet(entityNames);
		}
		final Set<String> entityMap = temSet;
		Map<String, IWrapperMapper> services = SpringUtil.getBeansOfType(IWrapperMapper.class);
		if (MapUtils.isEmpty(services)) {
			return;
		}
		//key = 类名 + "init" value = 数据集合
		Map<String, List> dataMap = Maps.newHashMap();

		//TO-DO 查出所有数据
		services.forEach((k,v)->{
			String key = StringUtils.join(v.entityClass().getName(),"init");

			DocumentBase document = getDocument(key, v.entityClass());
			if (document == null){
				return;
			}
			Document doc = document.getClass().getAnnotation(Document.class);
			String indexName = doc.indexName();
			boolean res1 = entityMap.contains(v.entityClass().getSimpleName());
			boolean res2 = entityMap.contains(indexName);
			boolean res3 = res1 || res2;
			if(entityMap != null && !res3){
				return;
			}
			List data =  null;

			if (!CollectionUtils.isEmpty(ids)) {
				PageHelper.clearPage();
				IQuery query = v.query();
				data = v.listEntity(query);
			}else{
				data = v.listByIds(ids);
			}

			if (CollectionUtils.isEmpty(data)) {
				return;
			}
			dataMap.put(key, data);
		});

		dataMap.forEach((k, v) -> {
			Stopwatch started = Stopwatch.createStarted();
			v.parallelStream().forEach(obj -> {
				DocumentBase document = getDocument(k, obj);
				if (document != null) {
					return;
				}
				save(document);
			});
			log.info("初始化索引完成{},{}，耗时：{}ms", k, v.size(), started.elapsed(TimeUnit.MILLISECONDS));
		});

	}
}
