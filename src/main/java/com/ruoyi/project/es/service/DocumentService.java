package com.ruoyi.project.es.service;

import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.es.domain.DocumentBase;
import com.ruoyi.project.es.domain.PageSearchVO;

import java.util.List;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 6:04 PM
 */
@SuppressWarnings("all")
public interface DocumentService {



	/**
	 * 查询文档
	 */

	public <T> TableDataInfo<List<T>> selectDocument(PageSearchVO pageRequest, Class<T> clz);

	/**
	 * 查询所有文档
	 */

	public <T> TableDataInfo<List<T>> selectDocument(PageSearchVO pageRequest);


	/**
	 * 保存文档
	 */

	void save(DocumentBase document);

	/**
	 * 删除文档
	 */

	void delete(DocumentBase document);

	/**
	 * 删除文档
	 */

	void delete(String id, Class clz);

	/**
	 * 获取文档
	 */

	DocumentBase getDocument(String key, Object obj);

	void init(List<String> entityNames, List<Long> ids);
}
