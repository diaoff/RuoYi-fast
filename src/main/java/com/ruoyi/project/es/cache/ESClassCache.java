package com.ruoyi.project.es.cache;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author diaoff
 * @version 1.0 2022/4/20
 * @date: 2022/4/20 5:59 PM
 */
@SuppressWarnings("all")
public class ESClassCache {

	private static Map<String, Class> cacheClasses = Maps.newConcurrentMap();

	/**
	 * 是否存在缓存
	 *
	 * @param key
	 * @return
	 */
	public static Boolean exists(String key) {
		return !(cacheClasses.containsKey(key) && cacheClasses.get(key) == Object.class);
	}

	public static Class getCacheDocumentClass(String key) {
		Class clz = cacheClasses.get(key);
		if (clz == Object.class) {
			return null;
		}
		return clz;
	}

	/**
	 * 添加缓存
	 * @param key
	 * @param clz
	 */
	public static void put(String key, Class clz) {
		if (clz == null) {
			clz = Object.class;
		}
		cacheClasses.put(key, clz);
	}
}
