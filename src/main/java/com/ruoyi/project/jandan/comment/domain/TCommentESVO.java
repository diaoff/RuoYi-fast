package com.ruoyi.project.jandan.comment.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.project.es.constant.PathConstant;
import com.ruoyi.project.es.domain.DocumentBase;
import com.ruoyi.project.jandan.images.domain.Images;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Setting;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 帖子对象 t_comment
 *
 * @author diaoff
 * @date 2021-07-27
 */
@Data
@Accessors(chain = true)
@Document(indexName = "jandan_comment")
//@Setting(settingPath = PathConstant.SETTING_PATH)
public class TCommentESVO extends DocumentBase {

	private static final long serialVersionUID = 1L;

	List<Images> images = new ArrayList();
	/**
	 * ID
	 */
	private Long id;
	/**
	 * 分区ID
	 */
	private Long postId;
	/**
	 * 作者
	 */
	private String author;
	/**
	 * 作者类型
	 */
	private Long authorType;
	/**
	 * 时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date date;
	/**
	 * 时间戳(秒)
	 */
	private Long dateUnix;
	/**
	 * 分区名
	 */
	private String postTitle;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 用户ID
	 */
	private Long userId;
	/**
	 * 赞
	 */
	private Long votePositive;
	/**
	 * 踩
	 */
	private Long voteNegative;

}
