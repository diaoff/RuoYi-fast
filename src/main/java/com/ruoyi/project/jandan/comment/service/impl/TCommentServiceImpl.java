package com.ruoyi.project.jandan.comment.service.impl;

import cn.org.atool.fluent.mybatis.If;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.project.jandan.comment.domain.TComment;
import com.ruoyi.project.jandan.comment.mapper.CommentMapper;
import com.ruoyi.project.jandan.comment.mapper.TCommentMapper;
import com.ruoyi.project.jandan.comment.service.ITCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 帖子Service业务层处理
 *
 * @author diaoff
 * @date 2021-07-27
 */
@Service
public class TCommentServiceImpl implements ITCommentService {
	@Autowired
	private CommentMapper commentMapper;

	@Autowired
	private TCommentMapper tCommentMapper;

	/**
	 * 查询帖子
	 *
	 * @param id 帖子ID
	 * @return 帖子
	 */
	@Override
	public TComment selectTCommentById(Long id) {
		return tCommentMapper.findById(id);
	}

	@Override
	public TComment selectTCommentImagesById(Long id) {
		return commentMapper.selectTCommentImagesById(id);
	}

	/**
	 * 查询帖子列表
	 *
	 * @param tComment 帖子
	 * @return 帖子
	 */
	@Override
	public List<TComment> selectTCommentList(TComment tComment) {
		return commentMapper.selectTCommentList(tComment);
	}

	/**
	 * 新增帖子
	 *
	 * @param tComment 帖子
	 * @return 结果
	 */
	@Override
	public int insertTComment(TComment tComment) {
		return tCommentMapper.insert(tComment);
	}

	/**
	 * 修改帖子
	 *
	 * @param tComment 帖子
	 * @return 结果
	 */
	@Override
	public int updateTComment(TComment tComment) {
		return tCommentMapper.updateById(tComment);
	}

	/**
	 * 删除帖子对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteTCommentByIds(String ids) {
		return tCommentMapper.deleteByIds(Arrays.asList(Convert.toStrArray(ids)));
	}

	/**
	 * 删除帖子信息
	 *
	 * @param id 帖子ID
	 * @return 结果
	 */
	@Override
	public int deleteTCommentById(Long id) {
		return tCommentMapper.deleteById(id);
	}

	/**
	 * 查询帖子列表
	 *
	 * @param tComment 帖子
	 * @return 帖子
	 */
	@Override
	public List<TComment> selectTCommentImagesList(TComment tComment) {
		return commentMapper.selectTCommentImagesList(tComment);
	}

	@Override
	public List<Long> selectTCommentIds(TComment tComment) {
		Date date = tComment.getDate();
		List<TComment> tComments = tCommentMapper.query().select.id().end().where.date().ge(date, If::isNull).end().to().listEntity();
		return tComments.stream().map(TComment::getId).collect(Collectors.toList());
	}
}
